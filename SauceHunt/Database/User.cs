using System;
using System.Collections.Generic;

namespace Database
{
    public class User
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual IList<RedditPost> Posts { get; set; }
        public virtual bool SkipWaitPeriod { get; set; }
        public virtual bool IgnoreUser { get; set; }
        public virtual bool WarnIfNoSauce { get; set; }
    }
}