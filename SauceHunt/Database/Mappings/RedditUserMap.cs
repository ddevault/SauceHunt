using System;
using FluentNHibernate.Mapping;

namespace Database.Mappings
{
    public class RedditUserMap : ClassMap<User>
    {
        public RedditUserMap()
        {
            Id(u => u.Id);
            Map(u => u.Name);
            Map(u => u.SkipWaitPeriod);
            Map(u => u.IgnoreUser);
            Map(u => u.WarnIfNoSauce);
            HasMany(u => u.Posts);
        }
    }
}