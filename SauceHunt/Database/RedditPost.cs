using System;

namespace Database
{
    public class RedditPost
    {
        public virtual int Id { get; set; }
        public virtual bool SauceFound { get; set; }
        public virtual string RedditId { get; set; }
        public virtual User Submitter { get; set; }
    }
}